ca.init();
var request_url = 'http://ol.fyitgroup.cn/index.php';
var isLoading = true;
localStorage.setItem('REQUEST_URL', request_url);

//获取token
function get_access_token(app_id,app_secret,uuid){
//	ca.ajaxSettings.async = false;
	ca.ajax({
		url:request_url+'/api/public/token',
		type:'get',
		data:{uuid:uuid,app_id:app_id,app_secret:app_secret},
		succFn:function(res){
			var json = JSON.parse(res);
			console.log(res);
			//获取token并保持到本地缓存中
			if(json.status == 1){
				//设置缓存
				localStorage.setItem('access_token', json.access_token);
			}
		}
	});
	
}
//初始化授权，获取token
function init_token(uuid){
	//设备号是否存在，不存在则无法请求 
	if(!uuid){
		ca.prompt('参数错误');return;
	}
	var app_id = 'appb9a83e5ee8125ba6';
	var app_secret = 'e74cded28821b575d5039764d736efae';
	var access_token = localStorage.getItem('access_token');
	var uuid;
	get_access_token(app_id,app_secret,uuid);
}
//
function get_user_id(){
	var access_token = localStorage.getItem('access_token');
	ca.post({
		url:request_url+'/api/user/check_user_token?access_token='+access_token,
		data:{user_access_token:get_user_access_token()},
		succFn:function(res){
			var json = JSON.parse(res);
			console.log(res);
			ca.prompt(json.msg);
		}
	});
}
//初始化
function init_home(){
	var user_access_token = localStorage.getItem('user_access_token');
	var am_header = ca.className('am-header');
	var obj;
	if(user_access_token){
		obj = am_header[0];
		get_user_info(function(res){
			//console.log(res.);
			//console.log(res.avatar);
			localStorage.setItem('user_mobile', res.mobile);
			//缓存头像
			var avatar = res.avatar ? res.avatar : '../images/default.png';
			localStorage.setItem('user_avatar', avatar);
			//缓存昵称
			localStorage.setItem('user_nicename', res.user_nicename);
			//缓存性别
			localStorage.setItem('user_sex', res.sex);
			//缓存生日 
			localStorage.setItem('user_birthday', res.birthday);
			ca.id('mui_header_img').src = avatar;
			ca.id('mui_header_name').innerText = res.user_nicename;
		});
	}
	else{
		//默认未登录
		obj = am_header[1];
	}
	//先删除class
	removeClass(am_header[0],'mui-header-show');
	removeClass(am_header[1],'mui-header-show');
	//后添加class
	addClass(obj,'mui-header-show');
}
//获取用户信息home.html
function get_user_info(callBack){
	var access_token = localStorage.getItem('access_token');
	ca.post({
		url:request_url+'/api/oauth/get_user_info?access_token='+access_token,
		data:{user_access_token:get_user_access_token()},
		succFn:function(res){
			var json = JSON.parse(res);
			console.log(res);
			//ca.prompt(json.msg); 
			if(json.status == 1){
				callBack(json.info);
			}
			else if(json.status == -2){
				//删除用户授权token 
				localStorage.removeItem('user_access_token');
				//删除手机
				localStorage.removeItem('user_mobile');
				//删除头像
				localStorage.removeItem('user_avatar');
				//删除昵称
				localStorage.removeItem('user_nicename');
				//删除性别
				localStorage.removeItem('user_sex');
				//删除生日
				localStorage.removeItem('user_birthday');
				ca.prompt(json.msg);
				init_home();
				return;
			}
			else{
				ca.prompt(json.msg);return;
			}
		}
	});
}
//修改用户信息info.html
function update_user_info(json_data,callBack){
	var access_token = localStorage.getItem('access_token');
	ca.post({
		url:request_url+'/api/oauth/update_user_info?access_token='+access_token,
		data:json_data,
		succFn:function(res){
			var json = JSON.parse(res);
			console.log(res);
			if(json.status == 1){
				callBack(json);
			}
			else{
				ca.prompt(ca.msg);
			}
		}
	});
}
//我的信息info.html
function get_info(){
	//账号
	var mobile = localStorage.getItem('user_mobile');
	//ca.id('mui-navigate-mobile').innerText = mobile.replace(mobile.substr(3,4), '****');
	//头像
	ca.id('mui-face-pic').src = localStorage.getItem('user_avatar');
	//昵称
	ca.id('mui-navigate-nickname').innerText = localStorage.getItem('user_nicename');
	var user_sex = localStorage.getItem('user_sex');
	if(user_sex == 1){
		user_sex = '男';
	}
	else if(user_sex == 2){
		user_sex = '女';
	}
	else{
		user_sex = '请选择';
	}
	//性别
	ca.id('mui-navigate-xingbie').innerText = user_sex;
	//生日
	//ca.id('mui-navigate-birthday').innerText = localStorage.getItem('user_birthday');
}
//点赞
function dianzan(){
	var user_access_token = localStorage.getItem('user_access_token');
	//检测用户是否登录
	if(!user_access_token){
		login();
	}
	$.get({
		url:request_url+'/api/',
		data:{},
		succFn:function(){
			
		}
	});
}
function login(){
	ca.newInterface({url:'pages/login.html',id:'login'});
}
//判断是否存在class
function hasClass(obj, cls) {  
    return obj.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));  
}  
//添加class
function addClass(obj, cls) {  
    if (!this.hasClass(obj, cls)) obj.className += " " + cls;  
}
//删除class
function removeClass(obj, cls) {  
    if (hasClass(obj, cls)) {  
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');  
        obj.className = obj.className.replace(reg, ' ');  
    }  
}  
//class存在则删除，不存在则添加
function toggleClass(obj,cls){  
    if(hasClass(obj,cls)){  
        removeClass(obj, cls);  
    }else{  
        addClass(obj, cls);  
    }  
}
//检测是否登录
function get_user_access_token(){
	var user_access_token = localStorage.getItem('user_access_token');
	if(user_access_token){
		return user_access_token;
	}
	return false;
}
/**
 * 延迟加载
 */
function lazyLoading(className,callBack,time){
	if(!isLoading){
		return;
	}
	var time = time ? time : 500;
	isLoading = false;
	ca.showWaiting('正在加载...');
	setTimeout(function(){
		ca.className(className)[0].style.display = 'block';
		ca.closeWaiting();
		callBack();
	},time);
}
//获取轮播列表数据 
function init_banner(slider_name){
	var access_token = localStorage.getItem('access_token');
	console.log(request_url+'/api/public/get_slider_list?access_token='+access_token);
	ca.post({
		url:request_url+'/api/public/get_slider_list?access_token='+access_token,
		data:{slider_name:slider_name},
		succFn:function(res){
			var json = JSON.parse(res);
			if(json.status == 1){
				var result = json.result;
				var slide_html = '';
				var slide_indicator = ''; 
				for(var i=0;i<result.length;i++){
					//console.log(result[i].slide_pic);
					if(i == 0){
						slide_html += '<div class="mui-slider-item mui-slider-item-duplicate"><a href="#"><img src="'+result[result.length-1]['slide_pic']+'" /></a></div>';
						slide_indicator += '<div class="mui-indicator mui-active"></div>';
					}
					else{
						slide_indicator += '<div class="mui-indicator"></div>';
					}
					slide_html += '<div class="mui-slider-item"><a href="#"><img src="'+result[i]['slide_pic']+'" /></a></div>';
					if(i == result.length - 1){
						slide_html += '<div class="mui-slider-item mui-slider-item-duplicate"><a href="#"><img src="'+result[0]['slide_pic']+'" /></a></div>';
					}
				}
				console.log(slide_html);
				ca.className('mui-slider-group')[0].innerHTML = slide_html;
				ca.className('mui-slider-indicator')[0].innerHTML = slide_indicator; 
				//图片轮播
				ca.pictureScroll({
				    callback:function(pictureNumber){
					    //console.log(pictureNumber); 
				    },
				    isAutoScroll:true,
				    scrollTime:2,
				    className:'.mui-slider-banner'
				});
			}
		}
	});
}