<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: XiaoYuan <767684610@qq.com>
// +----------------------------------------------------------------------
/**
 * 开发者token验证
 * @param string $token
 * @return bool
 */
function check_app_token($app_token){
	$signature = $_GET["signature"];
	$timestamp = $_GET["timestamp"];
	$nonce = $_GET["nonce"];

	$tmpArr = array($app_token, $timestamp, $nonce);
	// use SORT_STRING rule
	sort($tmpArr, SORT_STRING);
	$tmpStr = implode( $tmpArr );
	$tmpStr = sha1( $tmpStr );

	if( $tmpStr == $signature ){
		return true;
	}else{
		return false;
	}
}
/**
 * 获取生成token,是否拥有权限访问
 * @param string $uuid
 * @param string $app_id
 * @param string $app_secret
 * @return $access_token
 */
function get_app_access_token($uuid, $app_id, $app_secret){
	$access_token = S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_A_').$uuid);   //这里appid和appsecret我写固定了，实际是通过客户端获取  所以这里我们可以做很多 比如判断appid和appsecret有效性等
	//这里token存在则直接返回，不存在则生成并设置，最后返回token
	if($access_token){
		return $access_token;//S($ori_str,null);
	}

	//这里是token产生的机制  您也可以自己定义
	//$nonce = create_noncestr(32);
	$tmpArr = array($uuid,$app_id,$app_secret);
	sort($tmpArr, SORT_STRING);
	$tmpStr = implode( $tmpArr );
	$access_token = sha1( C('APP_AUTH_CODE').$tmpStr );
	//这里做了缓存 'a'=>b 和'b'=>a格式的缓存
	//S($this->app_id.'_'.$this->app_secret,$tmpStr,7200);
	S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_A_').$uuid, $access_token, 86400*180);
	S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_B_').$access_token, $uuid, 86400*180);
	return $access_token;
}
/**
 * 获取设备号
 * @param string $access_token
 * @return uuid
 */
function get_device_uuid($access_token){
	return S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_B_').$access_token);
}
/**
 * 获取生成用户登录token
 * @param integer $userid
 * @param string $app_access_token
 * @return $access_token
 */
function get_user_access_token($userid, $app_access_token){
	$access_token = S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_A_').$userid);
	//这里token存在则直接返回，不存在则生成并设置，最后返回token
	if($access_token){
		$tempToken = S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_U_').$access_token);
		//$tempToken = $tempToken.$app_access_token;
		//$tempToken1 = json_decode($tempToken,TRUE);
		//echo json_encode(array('status'=>1,'acc'=>$tempToken['access_token']));exit;
		if($tempToken && $tempToken['access_token'] != $app_access_token){
			
			//删除缓存，这里如果多设备登录，则删除之前设备的token。
			S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_A_').$userid, null);
			S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_U_').$access_token, null);
			return get_user_access_token($userid, $app_access_token);
		}
		//缓存+7天
		S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_A_').$userid, $access_token, 60*60*24*7);
		S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_U_').$access_token, json_encode(array('user_id'=>$tempToken['user_id'],'access_token'=>$tempToken['access_token'])), 60*60*24*7);
		return $access_token;//S($ori_str,null);
	}
	//这里是token产生的机制  您也可以自己定义
	$nonce = create_noncestr(32);
	$timestamp = time();
	$tmpArr = array($userid,$timestamp,$nonce);
	sort($tmpArr, SORT_STRING);
	$tmpStr = implode( $tmpArr );
	$access_token = sha1( C('APP_AUTH_CODE').$tmpStr );
	//S($this->app_id.'_'.$this->app_secret,$tmpStr,7200);
	//设置用户缓存，7天有效
	//S($access_token, 'app_access_token_'.$uuid, 60*60*24*7);
	//$access_token = json_encode(array('access_token'=>$temp_token,'user_id'=>$userid));
	//这里做了缓存 'a'=>b 和'b'=>a格式的缓存
	//缓存token
	S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_A_').$userid, $access_token, 60*60*24*7);
	//缓存用户id
	S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_U_').$access_token, json_encode(array('user_id'=>$userid,'access_token'=>$app_access_token)), 60*60*24*7);
	return $access_token;
}
/**
 * 获取用户id
 * @param string $user_access_token
 * @return integer user_id
 */
function get_user_id($user_access_token){
	$data = json_decode(S(C('APP_AUTH_CODE').C('APP_ACCESS_TOKEN_USER_U_').$user_access_token), true);
	if($data){
		//存在则累加有效期
		get_user_access_token($data['user_id'], $data['access_token']);
		return $data['user_id'];
	}
	return false;
}
/**
 * 作用：产生随机字符串，不长于32位
 * @param integer $length
 * @return string $str
 */
function create_noncestr( $length = 32 ){
	$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
	$str ="";
	for ( $i = 0; $i < $length; $i++ )  {
		$str.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
	}
	return $str;
}
/**
 * 获取文件地址
 * @param string $filename
 * @return string url
 */
function get_file_path($filename){
	$data = json_decode($filename,true);
	if(is_null($data)){
		return $filename ? C('APP_REQUEST_URL').$filename : '';		
	}
	return C('APP_REQUEST_URL').'/data/upload/'.$data['thumb'];
}