<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Author: XiaoYuan <767684610@qq.com>
// +----------------------------------------------------------------------
/**
 * 用户授权访问
 * 创建日期：2017-04-24
 */
namespace Api\Controller;
use Think\Controller;
class OauthController extends AppController{
	protected $users_model;
	protected $_users_access_token;
	function __construct(){
		parent::_initialize();
		//授权访问
		parent::checkAppToken();
		//用户登录
		parent::checkUserToken();
		$this->users_model = M('Users');
		$this->_users_access_token = $_REQUEST['user_access_token'];
	}
	public function index(){
		$data['token'] = $this->get_token();
		$this->apiReturn($data, '非法请求', '400');
    }
    //收藏
    public function my_favorites(){
    	
    }
    /**
     * 点赞
     * @param integer object_id
     * @return json
     */
    public function do_posts_likes(){
    	$posts_likes_model = M('posts_likes');
    	$object_id = I('post.object_id');
    	$user_id = get_user_id($this->_users_access_token);
    	if($posts_likes_model->where(array('uid'=>$user_id, 'object_id'=>$object_id))->find()){
    		$this->apiError('您已经点过赞了哦~');
    	}
    	$data['uid'] = $user_id;
    	$data['object_id'] = $object_id;
    	$data['create_time'] = time();
    	$data['status'] = 1;
    	$res = $posts_likes_model->add($data);
    	if($res){
    		M('posts')->where(array('id'=>$object_id))->setInc('post_like', 1);
    		$this->apiSuccess('点赞成功');
    	}
    	$this->apiError('点赞失败');
    }
    //获取用户信息
    public function get_user_info(){
    	//获取用户id
    	$user_id = get_user_id($this->_users_access_token);
    	$user_info = $this->users_model->where(array('id'=>$user_id))->field('mobile,user_nicename,avatar,sex,birthday,signature')->find();
    	if($user_info){
    		$user_info['avatar'] = get_file_path($user_info['avatar']);
    		$data['info'] = $user_info;
    		$this->apiReturn($data);
    	}
    	else{
    		$this->apiError('获取失败');
    	}
    }
    //更新用户信息
    public function update_user_info(){
    	$data = array();
    	if(isset($_POST['user_nicename'])){
    		$data['user_nicename'] = I('post.user_nicename');
    	}
    	if(isset($_POST['avatar'])){
    		$data['avatar'] = I('post.avatar');
    	}
    	if(isset($_POST['sex'])){
    		$data['sex'] = I('post.sex');
    	}
    	if(isset($_POST['birthday'])){
    		$data['birthday'] = I('post.birthday');
    	}
    	$this->users_model->where(array('id'=>get_user_id($this->_users_access_token)))->save($data);
    	$this->apiSuccess('修改成功');
    }
    //上传头像
    public function upload_avatar(){
    	$config=array(
    			'FILE_UPLOAD_TYPE' => sp_is_sae()?"Sae":'Local',//TODO 其它存储类型暂不考虑
    			'rootPath' => './'.C("UPLOADPATH"),
    			'savePath' => './avatar/',
    			'maxSize' => 5242880,//5M
    			'saveName'   =>    array('uniqid',''),
    			'exts'       =>    array('jpg', 'png', 'jpeg'),
    			'autoSub'    =>    false,
    	);
    	$upload = new \Think\Upload($config);//
    	$info=$upload->upload();
    	//开始上传
    	if ($info) {
    		//上传成功
    		$file= '/data/upload/avatar/'.$info['uploadkey']['savename'];
    		//图片处理，裁剪图片
    		$image = new \Think\Image();
    		//打开图片
    		$image->open('.'.$file);
    		//裁剪和压缩图片
    		$image->crop(I('get.w'), I('get.h'), I('get.x'), I('get.y'), 120, 120)->save('.'.$file);
			//$this->apiSuccess($info['uploadkey']);
			$user_id = get_user_id($this->_users_access_token);
			$this->users_model->where(array('id'=>$user_id))->setField('avatar',$file);
    		$this->apiReturn(array('save_url'=>get_file_path($file)),'修改成功');
    	} else {
    		//上传失败，返回错误
    		$this->apiError($upload->getError());
    	}
    }
}