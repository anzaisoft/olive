<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Author: XiaoYuan <767684610@qq.com>
// +----------------------------------------------------------------------
/**
 * 获取用户授权
 * 创建日期：2017-04-24
 */
namespace Api\Controller;
use Think\Controller;
class UserController extends AppController{
	protected $users_model;
	function __construct(){
		parent::_initialize();
		//授权访问
		parent::checkAppToken();
		//用户登录
		//parent::checkUserToken();
		$this->users_model = M('Users');
	}

	public function check_user_token(){
		!IS_POST && $this->apiError('非法请求');
		$user_id = $this->checkUserToken();
		if($user_id){
			$this->apiReturn(array('user_id'=>$user_id));
		}
		else{
			$this->apiError('用户未登录或已过期');
		}
	}
    /**
     * 用户注册
     * @param string mobile
     * @param string password
     */
	public function register(){
		$where['mobile'] = I('post.mobile');
		$where['password'] = sp_password(I('post.password'));
		if($this->users_model->where(array('mobile'=>$where['mobile']))->find()){
			$this->apiError('该手机号已被注册');
		}
		$this->_do_register();
	}
	/**
	 * 找回密码
	 * @param string mobile
	 * @param string password
	 * @return json
	 */
	public function find_password(){
		$mobile = I('post.mobile');
		$password = sp_password(I('post.password'));
		$res = $this->users_model->where(array('mobile'=>$mobile))->setField('user_pass', $password);
		$this->apiSuccess('密码修改成功');
	}
	/**
	 * 用户登录授权，生成token
	 * @param string mobile
	 * @param string password
	 * @return json
	 */
	public function user_oauth(){
		$where['mobile']=I('post.mobile');
		$password=I('post.password');
		$result = $this->users_model->where($where)->find();
		if(!empty($result)){
			if(sp_compare_password($password, $result['user_pass'])){
				//写入此次登录信息
				$data = array(
						'last_login_time' => date("Y-m-d H:i:s"),
						'last_login_ip' => get_client_ip(0,true),
				);
				$this->users_model->where(array('id'=>$result["id"]))->save($data);
				$access_token = $_GET['access_token'];
				$return_data['user_access_token'] = get_user_access_token($result['id'], $access_token);
				$user_info = $this->users_model->where(array('mobile'=>$where['mobile']))->field('mobile,user_nicename,avatar,sex,birthday,signature')->find();
				if($user_info){
					$user_info['avatar'] = get_file_path($user_info['avatar']);
    				$return_data['info'] = $user_info;
					$this->apiReturn($return_data, '登录成功');
				}
				else{
					$this->apiError('获取失败');
				}
			}else{
				$this->apiError('手机号或密码错误');
			}
		}else{
			$this->apiError("该手机号还没有注册");
		}
    }
	/**
	 * 用户注册
	 * @param string mobile
	 * @param string password
	 * @return json
	 */
    protected function _do_register(){
    	$rules = array(
    			//array(验证字段,验证规则,错误提示,验证条件,附加规则,验证时间)
    			array('mobile', 'require', '手机号不能为空！', 1 ),
    			//array('user_nicename', 'require', '昵称不能为空！', 1 ),
    			array('password','require','密码不能为空！',1),
    	);
    	if($this->users_model->validate($rules)->create()===false){
    		$this->apiError($this->users_model->getError());
    	}
    	$password = I('post.password');
    	$mobile = I('post.mobile');
    	
    	if(strlen($password) < 6 || strlen($password) > 18){
    		$this->apiError("密码长度至少6位，最多16位");
    	}
    	$where['mobile'] = $mobile;
    	
    	$result = $this->users_model->where($where)->count();
    	if($result){
    		$this->apiError("该手机号已被注册");
    	}else{
    	
    		$data=array(
    				'user_login' => '',
    				'user_email' => $mobile,
    				'mobile' =>$mobile,
    				'user_nicename' =>$mobile,
    				'user_pass' => sp_password($password),
    				'last_login_ip' => get_client_ip(0,true),
    				'create_time' => date("Y-m-d H:i:s"),
    				'last_login_time' => date("Y-m-d H:i:s"),
    				'user_status' => 1,
    				"user_type"=>2,//会员
    		);
    		$userid = $this->users_model->add($data);
    		if($userid){
    			//注册成功
    			//$return_data['user_access_token'] = get_user_access_token($userid, $_GET['access_token']);
    			//$user_info = $this->users_model->where(array('id'=>$userid))->field('mobile,user_nicename,avatar,sex,birthday,signature')->find();
    			//$return_data['info'] = $user_info;
    			$this->apiSuccess('注册成功');
    		}else{
    			$this->apiError('注册失败');
    		}
    	}
    }
}