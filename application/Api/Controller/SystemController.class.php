<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.anzaisoft.net All rights reserved.
// +----------------------------------------------------------------------
// | Author: ZhangQingYuan <767684610@qq.com>
// +----------------------------------------------------------------------
namespace Api\Controller;
use Think\Controller;
class SystemController extends Controller {
    function __construct(){
        header("Content-Type:text/html;charset=utf-8");
        //限制ip访问
        $list = array('211.152.53.68', '127.0.0.1', '139.196.38.222');
        if(!in_array(get_client_ip(), $list)){
            //\Think\Log::write(get_client_ip());exit;
        }
    }
    public function send_email(){
    	$result = sp_send_email('767684610@qq.com', '11111', '3213123123');
    	echo json_encode($result);
    }
}