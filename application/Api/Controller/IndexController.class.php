<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Author: XiaoYuan <767684610@qq.com>
// +----------------------------------------------------------------------
/**
 * 访问入口
 * 创建日期：2017-04-24
 */
namespace Api\Controller;
use Think\Controller;
class IndexController extends AppController{
	function __construct(){
		parent::_initialize();
	}
	public function index(){
		//dump(json_decode('{"user_id":"31","access_token":"3e90ba788d1cdf475ae90c6bd7859fb8fcd26415"}',true));
		//echo json_encode(S('app_access_token_user_u_b6e622706c00dd770c30b75c48dbd7d7ec1370de'));
		echo '{"msg":"error","status":"0"}';
	}
}