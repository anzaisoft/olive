<?php
// +----------------------------------------------------------------------
// | AnzaiSoft [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2015-2016 http://www.anzaisoft.net All rights reserved.
// +----------------------------------------------------------------------
// | Author: ZhangQingYuan <767684610@qq.com>
// +----------------------------------------------------------------------
namespace Admin\Controller;
use Common\Controller\AdminbaseController;
class OrderController extends AdminbaseController {
	protected $order_model;
	function _initialize() {
		parent::_initialize();
		$this->order_model = D("Common/Order");
	}
	/**
	 * 全部订单
	 */
    public function index(){
    	//待付款，待发货，已完成
    	$this->_lists();
		$this->display();
    }
    private function _lists(){
    	$params['1'] = '1';
    	if(IS_POST){
    		if(!empty($_POST['export'])){
    			$this->export();exit;
    		}
    		if (!empty($_POST['status'])) {
    			$params['status'] = I('post.status');
    		}
    		if (!empty($_POST['order_id'])) {
    			$params['order_id'] = array('like', '%'.I('order_id').'%');
    		}
    		if (!empty($_POST['start_time']) && !empty($_POST['end_time'])) {
    			$start_time = strtotime(I('start_time'));
    			$end_time = strtotime(I('end_time'));
    			$params['create_time'] = array('between',array($start_time, $end_time));
    		}
    		$this->post_data = I('post.');
    	}
    	/*
    	$count = M()->table('__ORDER__ a,__ORDER_TICKET__ b,__TICKET__ c')
    		->where(array('a.order_id = b.order_id', 'b.ticket_id = c.ticket_id', $params))
    		->count();
    	$page = $this->page($count, 20);
    	$order_list = M()->table('__ORDER__ a,__ORDER_TICKET__ b,__TICKET__ c')
		    	->where(array('a.order_id = b.order_id', 'b.ticket_id = c.ticket_id', $params))
		    	->order('a.create_time desc')
		    	->field('a.id,a.uid,c.name,a.order_id,a.receiver_name,a.receiver_mobile,a.create_time,a.status')
		    	->limit($page->firstRow . ',' . $page->listRows)
		    	->select();
		*/
    	$count = M('order')->where($params)->count();
    	$page = $this->page($count, 20);
    	$order_list = M('order')
	    	->where($params)
	    	->order('create_time desc')
	    	->limit($page->firstRow . ',' . $page->listRows)
	    	->select();
    	$this->assign("page", $page->show('Admin'));
    	$this->assign("order_list", $order_list);
    }
    //发货单
    public function delivery_list(){
    	$this->_lists();
    	$this->display();
    }
    //配货单
    public function delivery_info(){
    	echo $this->fetch();
//     	$this->display();
    }
    //打印订单
    public function order_print(){
    	$this->display();
    }
    //开票管理
    public function invoice(){
    	$params['1'] = '1';
    	if(IS_POST){
    		if(!empty($_POST['export'])){
    			$this->export();exit;
    		}
    		if (!empty($_POST['status'])) {
    			$params['status'] = I('post.status');
    		}
    		if (!empty($_POST['order_id'])) {
    			$params['order_id'] = array('like', '%'.I('order_id').'%');
    		}
    		if (!empty($_POST['start_time']) && !empty($_POST['end_time'])) {
    			$start_time = strtotime(I('start_time'));
    			$end_time = strtotime(I('end_time'));
    			$params['create_time'] = array('between',array($start_time, $end_time));
    		}
    		$this->post_data = I('post.');
    	}
    	$count = M('order')->where($params)->count();
    	$page = $this->page($count, 20);
    	$order_list = M('order')
    	->where($params)
    	->order('create_time desc')
    	->limit($page->firstRow . ',' . $page->listRows)
    	->select();
    	$this->assign("page", $page->show('Admin'));
    	$this->assign("order_list", $order_list);
    	$this->display();
    }
    //是否开票
    public function do_invoice(){
    	M('order')->where(array('id'=>I('id')))->setField('isxuyaofapiao', I('is_invoice'));
    	$this->success('操作成功！');
    }
    public function add(){
    	if(IS_POST){
    		$data = I('options');
    		if(!empty($data['id'])){
    			$rst2 = $this->weixin_options->save($data);
    		}else{
    			$rst2 = $this->weixin_options->add($data);
    		}
    		if ($rst2!==false) {
    			$this->success("保存成功！");
    		} else {
    			$this->error("保存失败！");
    		}
    	}
    	if(isset($_GET['id'])){
    		$this->options = $this->weixin_options->where(array('id'=>I('id')))->find();
    	}
    	$this->display();
    }
    public function index_post(){
    	$data= $_POST['options'];
    	if(!empty($data['id'])){
    		$rst2 = $this->weixin_options->save($data);
    	}else{
    		$rst2 = $this->weixin_options->add($data);
    	}
    	if ($rst2!==false) {
    		$this->success("保存成功！",U('Weixin/index'));
    	} else {
    		$this->error("保存失败！");
    	}
    }
    /**
     *查看订单信息
     */
    public function detail(){
    	!IS_AJAX && exit;
    	$params['order_id'] = I('oderd_id');
    	$this->order_list = M('order_ticket')->where($params)->select();
    	echo $this->fetch();
    }
    /**
     * 导出订单
     **/
    public function export(){
    	!IS_GET && exit;
    	$xlsName  = '订单报表_'.date('Ymd');
    	$xlsCell  = array(
    			array('id','序号'),
    			array('name','门票名称'),
    			array('order_id','订单号'),
    			array('username','姓名'),
    			array('mobile','手机号'),
    			array('create_time','交易时间'),
    	);
    	$params['1'] = '1';
    	if($_REQUEST['status']){
    		$params['a.status'] = I('status');
    	}
    	if(!empty($_REQUEST['order_id'])){
    		$params['a.order_id'] = array('like', '%'.I('order_id').'%');
    	}
    	if(!empty($_REQUEST['start_time']) && !empty($_REQUEST['end_time'])){
    		$start_time = strtotime(I('start_time'));
    		$end_time = strtotime(I('end_time'));
    		$params['a.create_time'] = array('between', "$start_time,$end_time");
    	}
    	$order_list = M()->table('__ORDER__ a,__ORDER_TICKET__ b,__TICKET__ c')
		    	->where(array('a.order_id = b.order_id', 'b.ticket_id = c.ticket_id', $params))
		    	->order('a.create_time asc')
		    	->field('a.id,c.name,a.order_id,a.receiver_name,a.receiver_mobile,a.create_time,a.uid')
		    	->select();
    	foreach ($order_list as $k => $v)
    	{
    		$order_list[$k]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
    		$order_list[$k]['username'] = get_user_info($v['uid'], 'user_nicename');
    		$order_list[$k]['mobile'] = get_user_info($v['uid'], 'mobile');
    		$order_list[$k]['order_id'] = ' '.$v['order_id'];
    	}
    	$this->exportExcel($xlsName,$xlsCell,$order_list);
    }
    /**
     * @param $expTitle 名称
     * @param $expCellName 参数
     * @param $expTableData 内容
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function exportExcel($expTitle,$expCellName,$expTableData){
    
    	$xlsTitle = iconv('utf-8', 'gb2312', $expTitle);//文件名称
    	$fileName = date('YmdHis');//or $xlsTitle 文件名称可根据自己情况设定
    	$cellNum = count($expCellName);
    	$dataNum = count($expTableData);
    	vendor("PHPExcel");
    
    	$objPHPExcel = new \PHPExcel();
    
    	$cellName = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
    	//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(100);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
    	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(22);
    	//        $objPHPExcel->getActiveSheet(0)->mergeCells('A1:'.$cellName[$cellNum-1].'1');//合并单元格
    	//         $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', $expTitle.'  Export time:'.date('Y-m-d H:i:s'));
    	for($i=0;$i<$cellNum;$i++){
    		$objPHPExcel->setActiveSheetIndex(0)->setCellValue($cellName[$i].'1', $expCellName[$i][1]);
    	}
    	// Miscellaneous glyphs, UTF-8
    	for($i=0;$i<$dataNum;$i++){
    		for($j=0;$j<$cellNum;$j++){
    			$objPHPExcel->getActiveSheet(0)->setCellValue($cellName[$j].($i+2), $expTableData[$i][$expCellName[$j][0]]);
    		}
    	}
    	header('pragma:public');
    	header('Content-type:application/vnd.ms-excel;charset=utf-8;name="'.$xlsTitle.'.xls"');
    	header("Content-Disposition:attachment;filename=$fileName.xls");//attachment新窗口打印inline本窗口打印
    	$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');//Excel5为xls格式，excel2007为xlsx格式
    	$objWriter->save('php://output');
    	exit;
    }
   
}